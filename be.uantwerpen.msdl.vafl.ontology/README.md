## The System Ontology Bundle

## The Context Ontology Bundle

## The Validity Ontology Bundle

## Ontological Dependencies on Standards

## Annotations

| Vocabulary Bundle Annotations     | Semantics                                                                    |
|-----------------------------------|------------------------------------------------------------------------------|
| @dc:title                         | The title of the bundle                                                      |
| @dc:publisher                     | The organization that publishes the bundle                                   |
| @dc:^description                  | Brief description of the bundle                                              |
| @owl:deprecated (optional)        | Bundle is deprecated                                                         |
| @owl:priorVersion (optional)      | The previous version (if exists) of the bundle (format as NAMESPACE)         |
| @owl:versionInfo                  | The version identifier of the bundle                                         |
| @resource:laterVersion (optional) | The later version (if exists) of the deprecated bundle (format as NAMESPACE) |
| @dc:creator                       | The organization/person that created the bundle (1 per annotation)           |
| @dc:contributor                   | The organization/person that contributed to the bundle  (1 per annotation)   |
| @rdfs:comment (optional)          | Comment about the ontology bundle                                            |

| Vocabulary Annotations            | Semantics                                                                            |
|-----------------------------------|--------------------------------------------------------------------------------------|
| @dc:title                         | The title of the ontology                                                            |
| @dc:publisher                     | The organization that publishes the ontology                                         |
| @dc:^description                  | Brief description of the ontology                                                    |
| @owl:deprecated (optional)        | Ontology is deprecated                                                               |
| @owl:priorVersion (optional)      | The previous version (if exists) of the ontology (format as NAMESPACE)               |
| @owl:versionInfo                  | The version identifier of the ontology                                               |
| @resource:laterVersion (optional) | The later version (if exists) of a deprecated ontology (format as NAMESPACE)         |
| @dc:creator                       | The organization/person that created the ontology                                    |
| @dc:contributor (optional)        | The organization/person that contributed to the ontology                             |
| @rdfs:comment (optional)          | Comment about the ontology                                                           |
| @rdfs:isDefinedBy (optional)      | Document that defines concepts, from which the ontology is generated (format as DoI) |
| @rdfs:seeAlso (optional)          | Document related to the ontology (format as DoI)                                     |


| Type Annotations                  | Semantics                                                                        |
|-----------------------------------|----------------------------------------------------------------------------------|
| @rdfs:label                       | True name of the type                                                            |
| @resource:aka (optional)          | Other names of the type                                                          |
| @dc:^description                  | Brief description of the type class                                              |
| @resource:illustration (optional) | Illustrative example of an individual of the type                                |
| @owl:deprecated (optional)        | Type is deprecated                                                               |
| @owl:priorVersion (optional)      | The previous version (if exists) of the type (format as NAMESPACE)               |
| @owl:versionInfo (optional)       | The version identifier of the type                                               |
| @resource:laterVersion (optional) | The later version (if exists) of a deprecated type (format as NAMESPACE)         | 
| @rdfs:comment (optional)          | Comment about the type                                                           |
| @rdfs:isDefinedBy (optional)      | Document that defines concepts, from which the type is generated (format as DoI) |
| @rdfs:seeAlso (optional)          | Document related to the type (format as DoI)                                     |
