## Validity Frame Ontology

The ontology bundle consists of many sub-ontologies as described below:

- resource.oml : Describes the top-level entities
- actor.oml : Describes concepts and relations of actors and access control
- human.oml : Describes conceps and relations of human elements like stakeholders, organizations, persons, owners
- system.oml : Describes concepts and relations of system, asset, system-under-study, environment, and their relations 
- context.oml : Describes concepts and relations of context
- requirement.oml : Describes concepts and relations of goal and requirement
- language.oml : Describes concepts and relations of language, syntax, formalism, and semantics
- model.oml : Describes concepts and relations of model
- mpm.oml : Describes concepts and relations of paradigm, view
- experiment.oml : Describes concepts and relations of experiments
- validity.oml : Describes concepts of validity
- vim4uncertainty.oml : Describes extension of the org.bipm.jcgm:vim4 ontology with concepts of measurement uncertainty
- vim4devices.oml : Describes extension of the org.bipm.jcgm:vim4 ontology with concepts of measurement devices
- workflow.oml : Describes concepts and relations of experimental workflows