@dc:creator "Rakshit Mittal"
@dc:contributor "Hans Vangheluwe"
@dc:publisher "MSDL UAntwerpen Belgium"
@dc:title "Resource Vocabulary"
@dc:^description "This vocabulary describes the top-level entities related to resources."
@rdfs:label "resource_vocabulary"
vocabulary <http://msdl.uantwerpen.be/vocabulary/system/resource#> as resource {
	
	extends <http://www.w3.org/2001/XMLSchema#> as xsd
	extends <http://www.w3.org/2000/01/rdf-schema#> as rdfs
	extends <http://purl.org/dc/elements/1.1/> as dc	
	extends <http://msdl.uantwerpen.be/vocabulary/system/process#> as process

	annotation property aka < rdfs:comment
	annotation property illustration < rdfs:comment
	
	@dc:^description "The abstract class of all materials which help us satisfy our needs and wants."
	@rdfs:comment "This is the top-level entity."
	@rdfs:label "resource"
	aspect Resource < process:Artifact
	
	@dc:^description "The abstract class of elements that are uniquely identified by an id."
    @rdfs:label "identified_element"
    aspect IdentifiedElement [
        key hasId
    ] < Resource
    
    @dc:^description "The id property of an identifiedElement."
    scalar property hasId [
        domain IdentifiedElement
        range xsd:string
        functional
    ]
    
    @dc:^description "The abstract class of elements that have a (possibly non-unique) name."
    @rdfs:label "named_element"
    aspect NamedElement < IdentifiedElement [
    	restricts scalar property hasName to exactly 1
    ]
    
    @dc:^description "The name property of a NamedElement."
    scalar property hasName [
    	domain NamedElement
    	range xsd:string
    ]
}