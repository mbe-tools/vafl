## VaFL : Validity Frame Language

pronounced "waffle"

This repository contains 3 Eclipse projects:

1. be.uantwerpen.msdl.vafl.ontology : An OML project specifying the ontological metamodel on which VaFL is based.
2. be.uantwerpen.msdl.vafl.docgen : An Acceleo project to generate LaTeX documentation from the ontology. 
3. be.uantwerpen.msdl.vafl.docgen.latex : A TeXlipse project to build the generated LaTeX file and make a PDF.
4. be.uantwerpen.msdl.vafl.utils : Project containing utilitarian and related modeling resources.

This repository contains 4 other directories:

1. be.uantwerpen.msdl.vafl.exspec/ : This directory contains the grammar and compiler for the ExSpec : Experiment Specification Language
2. dissemination/ : This directory contains materials like figures and presentations for the dissemination of VaFL.
3. slr/ : This directory contains materials related to the Systematic Literature Review performed as a domain-scoping to construct VaFL.
4. case-studies/ : This directory contains code and data related to the following case-studies:
    1. notch_filter/ : Validation of Modelica models of an R-C Notch filter with notch frequency approx 100kHz.
    2. analog_computer/ : Experiments leading up to building an analog computer.
5. python3-venv/ : A minimal Python3 virtual environment to execute code related to the case-studies.
