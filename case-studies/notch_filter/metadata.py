import struct

import pyarrow
from pyarrow import Field

TRACE_TABLE_METADATA: dict = {b'output': struct.pack('i', 0),
                              b'input': struct.pack('i', 1)}
POI_TABLE_METADATA: dict = {b'gain_amplitude': struct.pack('i', 0),
                            b'gain_curvefit': struct.pack('i', 1),
                            b'gain_rms': struct.pack('i', 2),
                            b'phase_amplitude': struct.pack('i', 3),
                            b'phase_curvefit': struct.pack('i', 4)}
TRACE_COLUMN_NAME_TYPE_PREPEND: dict = {b'output': 'o_',
                                        b'input': 'i_'}
POI_COLUMN_NAME_PREPEND: str = 'poi_'
COMMON_COLUMN_METADATA: list = ['testFrequency', 'testAmplitude', 'samplingRatio', 'time']
TRACE_COLUMN_METADATA: list = [b'type']


def check_metadata_equality(metadata: dict,
                            test_frequency: float,
                            test_amplitude: float,
                            sampling_ratio: int) \
        -> bool:
    if (struct.unpack('d', metadata[bytes(COMMON_COLUMN_METADATA[0], 'utf-8')])[0] == test_frequency and
        struct.unpack('d', metadata[bytes(COMMON_COLUMN_METADATA[1], 'utf-8')])[0] == test_amplitude and
            struct.unpack('i', metadata[bytes(COMMON_COLUMN_METADATA[2], 'utf-8')])[0] == sampling_ratio):
        return True
    else:
        return False


def create_column_metadata(test_frequency: float,
                           test_amplitude: float,
                           sampling_ratio: int,
                           time: float) \
        -> dict:
    return {COMMON_COLUMN_METADATA[0]: struct.pack('d', test_frequency),
            COMMON_COLUMN_METADATA[1]: struct.pack('d', test_amplitude),
            COMMON_COLUMN_METADATA[2]: struct.pack('i', sampling_ratio),
            COMMON_COLUMN_METADATA[3]: struct.pack('d', time)}


def create_trace_fields(test_frequency: float,
                        test_amplitude: float,
                        sampling_ratio: int,
                        time: float) \
        -> list[Field]:
    fields = []
    column_metadata: dict = create_column_metadata(test_frequency, test_amplitude, sampling_ratio, time)
    for column_type in TRACE_TABLE_METADATA.keys():
        column_metadata["type"] = column_type
        fields.append(pyarrow.field(TRACE_COLUMN_NAME_TYPE_PREPEND[column_type] + str(time),
                                    pyarrow.float64(), False, metadata=column_metadata))
    return fields


def get_column_metadata(metadata: dict) \
        -> [float, float, int, float]:
    return [(struct.unpack('d', metadata[bytes(COMMON_COLUMN_METADATA[0], 'utf-8')]))[0],
            (struct.unpack('d', metadata[bytes(COMMON_COLUMN_METADATA[1], 'utf-8')]))[0],
            int((struct.unpack('i', metadata[bytes(COMMON_COLUMN_METADATA[2], 'utf-8')]))[0]),
            (struct.unpack('d', metadata[bytes(COMMON_COLUMN_METADATA[3], 'utf-8')]))[0]]


def get_poi_index(poi: str,
                  poi_method: str) \
        -> int:
    return int(struct.unpack('i', POI_TABLE_METADATA[bytes(poi + '_' + poi_method, 'utf-8')])[0])


def get_time_from_field(field: Field) -> float:
    return struct.unpack('d', field.metadata[bytes(COMMON_COLUMN_METADATA[3], 'utf-8')])[0]
