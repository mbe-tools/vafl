import math

import numpy
from scipy.optimize import curve_fit

import context
import data

POIS: list = ['gain', 'phaseDiff']
POI_METHODS: dict = {POIS[0]: ['amplitude', 'curvefit', 'rms'],
                     POIS[1]: ['amplitude', 'curvefit']}


def compute_poi(poi: str,
                poi_method: str,
                output_data: numpy.ndarray[float],
                input_data: numpy.ndarray[float],
                sampling_ratio: int,
                test_frequency: float) \
        -> float:
    if poi == POIS[0]:
        return compute_gain(poi_method, output_data, input_data, sampling_ratio, test_frequency)
    elif poi == POIS[1]:
        return compute_phasediff(poi_method, output_data, sampling_ratio, test_frequency)
    else:
        raise NotImplementedError("The requested poi is not implemented!")


# def compute_pois(output_data: numpy.ndarray[float],
#                  input_data: numpy.ndarray[float],
#                  sampling_ratio: int,
#                  test_frequency: float,
#                  return_poi: str,
#                  return_poi_methods: list[str]) \
#         -> numpy.ndarray[float]:
#     pois = []
#     for poi_method in return_poi_methods:
#         pois.append(compute_poi(return_poi, poi_method, output_data, input_data, sampling_ratio, test_frequency))
#     return numpy.array(pois)


def compute_all_pois(samples: numpy.ndarray[float],
                     sampling_ratio: int,
                     test_frequency: float,
                     test_amplitude: float = None,
                     time: float = None,
                     system: str = None,
                     save: bool = True) \
        -> numpy.ndarray[float]:
    all_pois: list[float] = []
    for poi in POIS:
        for poi_method in POI_METHODS[poi]:
            all_pois.append(compute_poi(poi, poi_method, samples[0], samples[1], sampling_ratio, test_frequency))
    if save:
        data.save_pois(numpy.array(all_pois), test_frequency, test_amplitude, sampling_ratio, time, system)
    return numpy.array(all_pois)


# Gain

def compute_gain(gain_method: str,
                 output_data: numpy.ndarray[float],
                 input_data: numpy.ndarray[float],
                 sampling_ratio: int = None,
                 test_frequency=None) \
        -> float:
    if gain_method == POI_METHODS[POIS[0]][0]:
        return compute_amplitude_gain(output_data, input_data)
    elif gain_method == POI_METHODS[POIS[0]][1]:
        return compute_curve_fit_gain(output_data, input_data, sampling_ratio, test_frequency)
    elif gain_method == POI_METHODS[POIS[0]][2]:
        return compute_rms_gain(output_data, input_data)
    else:
        raise NotImplementedError("The requested gain calculation method is not implemented!")


def compute_amplitude_gain(output_data: numpy.ndarray[float],
                           input_data: numpy.ndarray[float]) \
        -> float:
    return numpy.max(abs(output_data)) / numpy.max(abs(input_data))


def compute_curve_fit_gain(output_data: numpy.ndarray[float],
                           input_data: numpy.ndarray[float],
                           sampling_ratio: int,
                           test_frequency: float) \
        -> float:
    def sin_func(x: float, a: float, phi: float) -> float:
        return a * numpy.sin(2 * math.pi * (test_frequency * x + phi))

    time_data = generate_time_data(test_frequency, sampling_ratio)
    output_params: numpy.ndarray[float] = curve_fit(f=sin_func, xdata=time_data, ydata=output_data)[0]
    input_params: numpy.ndarray[float] = curve_fit(f=sin_func, xdata=time_data, ydata=input_data)[0]
    return abs(output_params.item(0) / input_params.item(0))


def compute_rms_gain(output_data: numpy.ndarray[float],
                     input_data: numpy.ndarray[float]) \
        -> float:
    return math.sqrt(numpy.square(output_data).mean() / numpy.square(input_data).mean())


# Phase Difference
def compute_phasediff(phasediff_method: str,
                      output_data: numpy.ndarray[float],
                      sampling_ratio: int,
                      test_frequency: float = None) \
        -> float:
    if phasediff_method == POI_METHODS[POIS[1]][0]:
        return compute_amplitude_phasediff(output_data, sampling_ratio)
    elif phasediff_method == POI_METHODS[POIS[1]][1]:
        return compute_curvefit_phasediff(output_data, sampling_ratio, test_frequency)
    else:
        raise NotImplementedError("The requested phase difference calculation method is not implemented!")


def compute_amplitude_phasediff(output_data: numpy.ndarray[float],
                                sampling_ratio: int) \
        -> float:
    output_index: int = numpy.argmax(output_data[:int(sampling_ratio)])
    if output_index < 3 * sampling_ratio / 4:
        return ((output_index / sampling_ratio) * 360) - 90
    else:
        return (360 * (output_index / sampling_ratio)) - (360 + 90)


def compute_curvefit_phasediff(output_data: numpy.ndarray[float],
                               sampling_ratio: int,
                               test_frequency: float) \
        -> float:
    def sin_func(x, a, phi):
        return a * numpy.sin(2 * math.pi * (test_frequency * x + phi))

    output_params = curve_fit(f=sin_func, xdata=generate_time_data(test_frequency, sampling_ratio),
                              ydata=output_data, bounds=[[-numpy.inf, -0.5], [numpy.inf, 0.5]])[0]
    return output_params.item(1) * 360


def generate_time_data(test_frequency: float,
                       sampling_ratio: int) \
        -> numpy.ndarray[float]:
    time_step: float = 1 / (test_frequency * sampling_ratio)
    return numpy.arange(context.OUTPUT_STABILISATION_TIME,
                        context.OUTPUT_STABILISATION_TIME + (context.DATA_CYCLES * sampling_ratio * time_step),
                        time_step)[:context.DATA_CYCLES*sampling_ratio]


# def generate_poi_table_metadata() -> dict:
#     poi_table_metadata: dict = {}
#     method_index = 0
#     for poi in POIS:
#         for poi_method in POI_METHODS[poi]:
#             poi_table_metadata[bytes(poi + '_' + poi_method, 'utf-8')] = struct.pack('i', method_index)
#             method_index += 1
#     return poi_table_metadata
