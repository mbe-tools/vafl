import logging

import numpy
from OMPython import ModelicaSystem

import data
import context
from data import delete_modelica_build_files


def single_simulation(model: ModelicaSystem,
                      test_frequency: float,
                      test_amplitude: float,
                      sampling_ratio: int,
                      model_name: str) \
        -> [float, numpy.ndarray[float]]:
    logging.info('Performing simulation: f:' + str(test_frequency) + ', A:' + str(test_amplitude) + ', R:' + str(sampling_ratio))
    sampling_frequency: float = test_frequency * sampling_ratio
    delete_files: bool = False
    if model is None:
#        omc = OMCSessionZMQ()
        model = ModelicaSystem('NotchFilter.mo', 'NotchFilter.' + model_name)
        model.buildModel('(V_in.v)|(V_out.v)')
        delete_files: bool = True
    model.setSimulationOptions(["stepSize=" + str(1/sampling_frequency),
                                "tolerance=1e-9",
                                "startTime=0",
                                "stopTime=" + str((2 * context.DATA_CYCLES) / test_frequency)])
    model.setParameters(['Vt=' + str(test_amplitude),
                         'Ft=' + str(test_frequency)])

    model.simulate()
    samples: numpy.ndarray[float] = numpy.array(model.getSolutions(["V_out.v", "V_in.v"]))
    sample_count = context.DATA_CYCLES * sampling_ratio
    samples = numpy.array([samples[0][-sample_count:],
                           samples[1][-sample_count:]]).round(8)
    if delete_files:
        delete_modelica_build_files()
    trace_time: float = data.save_simulation_trace(samples, test_frequency, test_amplitude, sampling_ratio, model_name)
    return trace_time, samples

# from pydelica import Session
# def singleSimulationPydelica(testFrequency, testAmplitude, samplingRatio):
#     with Session() as session:
#         session.build_model('notchFilter.mo','notchFilter')
#         session.set_parameter('Vt', testAmplitude)
#         session.set_parameter('Ft', testFrequency)
#         session.set_time_range(start_time=0, stop_time = outputStabilisationTime+(dataCycles/testFrequency))
#         session.set_variable_filter(filter_str='(V_in.v)|(V_out.v)')
#         session.simulate()
#     samples = numpy.transpose(session.get_solutions()['notchFilter'].to_numpy())
#     return samples
