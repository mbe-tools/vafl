import os
import struct

import enquiries
import numpy
import pyarrow
from pyarrow import parquet, Field
from tqdm import tqdm

import context
import data
import metadata
import process


def regenerate_all_poi_files() \
        -> None:
    regenerate_poi_files(context.SYSTEMS)


def regenerate_poi_files(systems: list[str]) \
        -> None:
    for system in tqdm(systems, desc='regenerating poi files of systems', leave=False):
        regenerate_poi_file(system)


def regenerate_poi_file(system: str):
    for amplitude_dir in os.listdir(system+'/'):
        if os.path.isdir(system+'/'+amplitude_dir):
            for file_name in os.listdir(system+'/'+amplitude_dir+'/'):
                if not file_name.startswith(data.POI_FILE_PREPEND):
                    trace_file_name: str = system + '/' + amplitude_dir + '/' + file_name
                    schema = parquet.read_schema(trace_file_name)
                    table = parquet.read_table(trace_file_name)
                    output_index = struct.unpack('i', metadata.TRACE_TABLE_METADATA[b'output'])[0]
                    input_index = struct.unpack('i', metadata.TRACE_TABLE_METADATA[b'input'])[0]
                    poi_table = None
                    for data_index in tqdm(range(0, table.num_columns, 2), desc='generating pois from ' + system, leave=False):
                        output_data = table.column(data_index + output_index).to_numpy()
                        input_data = table.column(data_index + input_index).to_numpy()
                        poi_metadata = schema.field(data_index).metadata
                        poi_metadata.pop(b'type')
                        test_frequency, test_amplitude, sampling_ratio, time = metadata.get_column_metadata(poi_metadata)
                        poi_field: Field = pyarrow.field(metadata.POI_COLUMN_NAME_PREPEND + str(time), pyarrow.float64(), False, metadata=poi_metadata)
                        poi: numpy.ndarray[float] = process.compute_all_pois(numpy.array([output_data, input_data]), sampling_ratio, test_frequency, save=False)
                        if poi_table is None:
                            poi_table = pyarrow.Table.from_arrays([poi], schema=pyarrow.schema([poi_field],
                                                                  metadata=metadata.POI_TABLE_METADATA))
                        else:
                            poi_table = poi_table.append_column(poi_field, [poi])
                    parquet.write_table(poi_table, system + '/' + amplitude_dir + '/' + data.POI_FILE_PREPEND + file_name)


if __name__ == "__main__":
    systems = enquiries.choose('re-generate PoIs from?', context.SYSTEMS, multi=True)
    regenerate_poi_files(systems)
