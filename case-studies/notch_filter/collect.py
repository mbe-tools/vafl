import os

import numpy
from pyarrow import Schema, parquet
from tqdm import tqdm

import data
import metadata
import perform
import process


def collect_traces(test_frequency: float,
                   test_amplitudes: numpy.ndarray[float],
                   sampling_ratios: numpy.ndarray[float],
                   systems: list) \
        -> [list[numpy.ndarray[float]], list[str], list[numpy.ndarray[float]]]:
    ratios_traces = []
    legend = []
    timescale = []
    for sampling_ratio in tqdm(sampling_ratios, desc='evaluating traces at sampling ratios', leave=False):
        traces = []
        for system in tqdm(systems, desc='evaluating traces from systems', leave=False):
            for test_amplitude in tqdm(test_amplitudes, desc='evaluating traces at amplitudes', leave=False):
                trace_time, trace = data.query_trace(test_frequency, test_amplitude, sampling_ratio, system)
                if trace is None:
                    print('found no trace! (f: ' + str(test_frequency) + 'Hz, A: ' + str(test_amplitude) + 'V, R: ' +
                          str(sampling_ratio) + '), performing experimental activity')
                    trace_time, trace = perform.perform_experimental_activity(system,
                                                                              numpy.array([test_frequency]),
                                                                              numpy.array([test_amplitude]),
                                                                              numpy.array([sampling_ratio]))
                traces.append(trace)
                legend.append('output: ' + system + ': f:' + str(test_frequency) + 'Hz, A:' + str(test_amplitude) + 'V, R:' + str(sampling_ratio))
                legend.append('input: ' + system + ': f:' + str(test_frequency) + 'Hz, A:' + str(test_amplitude) + 'V, R:' + str(sampling_ratio))
        ratios_traces.append(numpy.array(traces))
        timescale.append(process.generate_time_data(test_frequency, sampling_ratio))
    return [ratios_traces, legend, timescale]


def collect_poi_traces(test_frequencies: numpy.ndarray[float],
                       test_amplitudes: numpy.ndarray[float],
                       sampling_ratios: numpy.ndarray[int],
                       systems: list[str],
                       poi: str,
                       poi_methods: list[str]) \
        -> [numpy.ndarray[float], list[str]]:
    traces = []
    legend = []
    for system in tqdm(systems, desc='evaluating pois of different systems', leave=False):
        for sampling_ratio in tqdm(sampling_ratios, desc='evaluating pois at different sampling ratios', leave=False):
            for test_amplitude in tqdm(test_amplitudes, desc='evaluating pois at different amplitudes', leave=False):
                method_traces = []
                for test_frequency in tqdm(test_frequencies, desc='evaluating pois at different frequencies', leave=False):
                    pois: numpy.ndarray[float] = data.query_pois(test_frequency, test_amplitude, sampling_ratio, system, poi, poi_methods)
                    if pois is None:
                        print('found no pois! (f: ' + str(test_frequency) + 'Hz, A: ' + str(test_amplitude) + 'V, R: ' +
                              str(sampling_ratio) + '), checking if trace exists')
                        trace_time, trace = data.query_trace(test_frequency, test_amplitude, sampling_ratio, system)
                        if trace_time is None:
                            print('found no trace! (f: ' + str(test_frequency) + 'Hz, A: ' + str(test_amplitude) + 'V, R: ' +
                                  str(sampling_ratio) + '), initializing experimental activity')
                            trace_time, trace = perform.perform_experimental_activity(system,
                                                                                      numpy.array([test_frequency]),
                                                                                      numpy.array([test_amplitude]),
                                                                                      numpy.array([sampling_ratio]))
                        pois = process.compute_all_pois(trace, sampling_ratio, test_frequency, test_amplitude,
                                                        trace_time, system, save=True)
                    method_traces.append(pois)
                traces.append(numpy.transpose(method_traces))
                for poi_method in poi_methods:
                    legend.append(system + ': A:' + str(test_amplitude) + ', R:' + str(sampling_ratio) + ', method=' + poi_method)
    return [numpy.concatenate(traces, axis=0), legend]


def collect_trace_metadata(system: str) \
        -> [numpy.ndarray[float], numpy.ndarray[float], numpy.ndarray[int]]:
    test_frequencies: list[float] = []
    test_amplitudes: list[float] = []
    sampling_ratios: list[int] = []
    for amplitude_dir in os.listdir(system + '/'):
        if os.path.isdir(system + '/' + amplitude_dir):
            for file_name in os.listdir(system + '/' + amplitude_dir + '/'):
                if not file_name.startswith(data.POI_FILE_PREPEND):
                    trace_file_name: str = system + '/' + amplitude_dir + '/' + file_name
                    schema: Schema = parquet.read_schema(trace_file_name)
                    for data_index in range(0, parquet.read_table(trace_file_name).num_columns, 2):
                        test_frequency, test_amplitude, sampling_ratio, time = metadata.get_column_metadata(schema.field(data_index).metadata)
                        test_frequencies.append(test_frequency)
                        test_amplitudes.append(test_amplitude)
                        sampling_ratios.append(sampling_ratio)
    return [numpy.array(test_frequencies), numpy.array(test_amplitudes), numpy.array(sampling_ratios)]
