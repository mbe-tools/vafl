## Notch Filter Case-Study

This directory contains documentation, code, and data related to the notch
filter case-study. Note that to fetch the data, you should pull the remote repository using Git LFS.

A twin-T notch filter was created on a breadboard using discrete components at 
MSDL, University of Antwerp. Experiments on the notch-filter are performed and recorded
using Digilent Waveforms Analog Discovery 2 board on loan from COMELEC,
Telecom Paris. 

Corresponding Modelica models for an ideal and a non-ideal
physics-based theory were created. 

These 3 systems (experiment, ideal model, non-ideal model) have been tested using
Python3 packages PyDWF and OMPython. All the data is stored in the Apache Parquet file format using PyArrow. The minimal Python3 virtual environment that can be used to test the
systems is provided [here](https://gitlab.telecom-paris.fr/mbe-tools/vafl/-/tree/master/python3-venv).

### Resources in the directory

1. data-sheet*.pdf : data-sheets provided by the manufacturer of the
components used to create the notch filter circuit.
2. C*_impedance.png : measured impedance curves of the components, which
were used to create their non-ideal models.
3. NotchFilter.mo: Modelica package containing the notch-filter models used for simulation.
4. *.py : Python3 code to run the experiments/simulations. More details
about the software architecture follow.
5. *.log : log-files from the performance of experiments, used to keep track
of experiments performed on the server, and identify points of failure.
6. *.exspec : Experiment Specification files used to perform the expriments.
7. Experiment/ : directory that stores traces from performed experiments as a
distributed Apache Parquet dataset. 
8. IdealNotchFilterModel/ : directory that stores traces from performed simulations of the ideal notch-filter model as a
distributed Apache Parquet dataset. 
9. NonIdealNotchFilterModel/ : directory that stores traces from performed simulations of the non-ideal notch-filter model as a
distributed Apache Parquet dataset.

### Python modules

1. plot: The top-level module that is used to interact with the user and generate plot based on the options selected by the user.
The user is required to specify the kind of plot, and metrics associated with the experimental frame that the user is interested in plotting. Please note that for large numbers of data that has not been performed, it is advised to perform those experiments first through the perform module. If the PoIs are to be plotted, it is advised to first run the regenerate module and ensure that pois have been regenerated for the system under study.
2. collect: The plot module calls the collect module to collect the necessary data. The collect module will query the dataset for each requested frame through the data module. If the frame is not already tested, a new test (experiment/simulation) will be performed.
3. perform: This is the top-level module to perform experiments and simulations. Users should use this module and not the experiment or simulate modules to perform tests!
4. regenerate: This module when invoked, will regenerate the poi files for all the stored traces.
5. experiment: This module uses PyDWF to perform experiments. As soon as an experiment is performed, the traces are saved through the data module.
6. simulate: This module uses OMPython to perform experiments. As soon as a simulation is performed, the traces are saved through the data module.
7. process: This module defines the properties of interest (pois) and methods to calculate these pois.
8. data: This module is responsible for storing the race and poi data, and also querying that data.
9. metadata: This module defines the metadata stored in the parquet files for each column and each file.
