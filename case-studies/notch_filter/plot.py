import matplotlib
from matplotlib import pyplot
from mpl_toolkits import mplot3d
import numpy
import enquiries

import collect
import context
import process

matplotlib.use('TkAgg')

DATA_OPTIONS = ['experiment trace',  # line plot # fixed test frequency # varying amplitudes, systems
                'property of interest',  # line plot # fixed frequency range # varying amplitudes, systems
                'concrete experiment specification',  # scatter plot # all existing trace metadata
                'concrete validity frame']  # scatter plot # all existing poi data


def plot_concrete_frame(test_frequencies_list: list[numpy.ndarray[float]],
                        test_amplitudes_list: list[numpy.ndarray[float]],
                        sampling_ratios_list: list[numpy.ndarray[float]],
                        legend: list[str]) \
        -> None:
    figure = pyplot.figure()
    axes = pyplot.axes(projection="3d")
    for index in range(0, len(test_frequencies_list), 1):
        axes.scatter3D(test_frequencies_list[index], test_amplitudes_list[index], sampling_ratios_list[index],
                       label=legend[index])
    axes.legend()
    axes.set_xlabel('Frequency (Hz)')
    axes.set_ylabel('Amplitude (V)')
    axes.set_zlabel('Sampling Ratio (n)')
    axes.grid(True)
    pyplot.show()


def plot_pois(x_data: numpy.ndarray[float],
              y_data: numpy.ndarray[float],
              legend: list[str],
              y_label: str) \
        -> None:
    make_plot(x_data, y_data, legend, y_label=y_label, x_label='Frequency (Hz)')


def plot_traces(x_data: list[numpy.ndarray[float]],
                y_data: list[numpy.ndarray[float]],
                legend: list[str]) \
        -> None:
    figure, axis = pyplot.subplots()
    for ratios_index in range(len(y_data)):
        for signal_index in range(len(y_data[ratios_index])):
            for signal_type in [0, 1]:
                axis.plot(x_data[ratios_index], y_data[ratios_index][signal_index][signal_type])
    pyplot.xlabel('Time (s)')
    pyplot.ylabel('Voltage (V)')
    pyplot.legend(legend)
    pyplot.show()


def make_plot(x_data: numpy.ndarray[float],
              y_data: numpy.ndarray[float],
              legend: list[str],
              y_label: str,
              x_label: str) \
        -> None:
    figure, axis = pyplot.subplots()
    for index in range(len(y_data)):
        axis.plot(x_data, y_data[index])
    pyplot.xlabel(x_label)
    pyplot.ylabel(y_label)
    pyplot.legend(legend)
    pyplot.show()


def user_choice(question: str,
                options: list,
                multi: bool = False) \
        -> str | list[str]:
    response = enquiries.choose(question, options, multi=multi)
    if multi:
        for option in response:
            print(question + ': ' + option)
    else:
        print(question + ': ' + response)
    return response


def user_choice_amplitude() -> numpy.ndarray[float]:
    minimum_amplitude: float = float(input('Minimum signal amplitude (V): '))
    maximum_amplitude: float = float(input('Maximum signal amplitude (V): '))
    amplitude_resolution: float = float(input('Resolution of signal amplitude (V): '))
    return numpy.arange(minimum_amplitude, maximum_amplitude + amplitude_resolution, amplitude_resolution)


def user_choice_sampling_ratio() -> numpy.ndarray[int]:
    minimum_sampling_ratio: float = int(input('Minimum sampling ratio (int): '))
    maximum_sampling_ratio: float = int(input('Maximum sampling ratio (int): '))
    sampling_ratio_resolution: float = int(input('Resolution of sampling ratio (int): '))
    return numpy.arange(minimum_sampling_ratio, maximum_sampling_ratio + sampling_ratio_resolution, sampling_ratio_resolution)


if __name__ == "__main__":
    datatype: str = user_choice('plot data type?', DATA_OPTIONS)
    systems: list[str] = user_choice('data from which system/s?', context.SYSTEMS, multi=True)
    if datatype == DATA_OPTIONS[0]:
        test_frequency: float = float(input('Input signal frequency (Hz): '))
        test_amplitudes: numpy.ndarray[float] = user_choice_amplitude()
        sampling_ratios: numpy.ndarray[int] = user_choice_sampling_ratio()
        traces, legend, timescales = collect.collect_traces(test_frequency, test_amplitudes, sampling_ratios, systems)
        plot_traces(timescales, traces, legend)
    elif datatype == DATA_OPTIONS[1]:
        minimum_frequency: float = float(input('Minimum signal frequency (Hz): '))
        maximum_frequency: float = float(input('Maximum signal frequency (Hz): '))
        frequency_resolution: float = float(input('Resolution of signal frequency (Hz): '))
        testFrequencies: numpy.ndarray[float] = numpy.arange(minimum_frequency,
                                                             maximum_frequency + frequency_resolution,
                                                             frequency_resolution)
        test_amplitudes: numpy.ndarray[float] = user_choice_amplitude()
        sampling_ratios: numpy.ndarray[int] = user_choice_sampling_ratio()
        poi: str = user_choice('Property of interest?', process.POIS)
        poi_methods: list[str] = user_choice('PoI calculation method?', process.POI_METHODS[poi], multi=True)
        pois, legend = collect.collect_poi_traces(testFrequencies, test_amplitudes, sampling_ratios, systems, poi,
                                                  poi_methods)
        y_label: str
        if poi == process.POIS[0]:
            pois = 10 * numpy.log10(pois)
            y_label = 'Gain (dB)'
        else:
            y_label = 'Phase Difference (deg)'
        plot_pois(testFrequencies, pois, legend, y_label=y_label)
    elif datatype == DATA_OPTIONS[2]:
        test_frequencies_list: list[numpy.ndarray[float]] = []
        test_amplitudes_list: list[numpy.ndarray[float]] = []
        sampling_ratios_list: list[numpy.ndarray[int]] = []
        legend: list[str] = []
        for system in systems:
            test_frequencies, test_amplitudes, sampling_ratios = collect.collect_trace_metadata(system)
            test_frequencies_list.append(test_frequencies)
            test_amplitudes_list.append(test_amplitudes)
            sampling_ratios_list.append(sampling_ratios)
            legend.append(system)
        plot_concrete_frame(test_frequencies_list, test_amplitudes_list, sampling_ratios_list, legend)
#    elif datatype == DATA_OPTIONS[3]:
