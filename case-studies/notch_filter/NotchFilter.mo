package NotchFilter
  block NonIdealResistor "Non-ideal model of resistor at 100kHz, tested with 100Ohm resistor. Inductance is not included because device displays capacitive behavior."
    parameter Modelica.Units.SI.Capacitance Cp "Parasitic Parallel Capacitance";
    parameter Modelica.Units.SI.Inductance Lp "Parasitic Series Inductance";
    parameter Modelica.Units.SI.Resistance R "Resistance";
    Modelica.Electrical.Analog.Interfaces.PositivePin p "Positive electrical pin" annotation (
      Placement(transformation(extent={{-110,-10},{-90,10}})));
    Modelica.Electrical.Analog.Interfaces.NegativePin n "Negative electrical pin" annotation (
      Placement(transformation(extent={{90,-10},{110,10}})));
    Modelica.Electrical.Analog.Basic.Resistor resistor(R=R) annotation(
      Placement(transformation(origin = {40, -20}, extent = {{-18, -18}, {18, 18}})));
    Modelica.Electrical.Analog.Basic.Capacitor parasiticCapacitance(C=Cp) annotation(
      Placement(transformation(origin = {0, 20}, extent = {{-18, -18}, {18, 18}})));
  equation
    connect(parasiticCapacitance.p, p) annotation(
      Line(points = {{-18, 20}, {-80, 20}, {-80, 0}, {-100, 0}}, color = {0, 0, 255}));
    connect(parasiticCapacitance.n, n) annotation(
      Line(points = {{18, 20}, {80, 20}, {80, 0}, {100, 0}}, color = {0, 0, 255}));
    connect(resistor.n, n) annotation(
      Line(points = {{58, -20}, {80, -20}, {80, 0}, {100, 0}}, color = {0, 0, 255}));
  connect(resistor.p, p) annotation(
      Line(points = {{22, -20}, {-80, -20}, {-80, 0}, {-100, 0}}, color = {0, 0, 255}));
    annotation (
      Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
              100}}), graphics={
          Rectangle(
            extent={{-70,30},{70,-30}},
            lineColor={0,0,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Line(points={{-90,0},{-70,0}}, color={0,0,255}),
          Line(points={{70,0},{90,0}}, color={0,0,255}),
          Text(
            extent={{-150,-40},{150,-80}},
            textString="R=%R"),
          Text(
            extent={{-150,90},{150,50}},
            textString="%name",
            textColor={0,0,255})}));
  
  end NonIdealResistor;

  block NonIdealCapacitor "Non-ideal model of capacitor at 100kHz, tested with 1kOhm resistor. Inductance is not included because device displays capacitive behavior."
    parameter Modelica.Units.SI.Resistance Rp "Parasitic Series Resistance";
    parameter Modelica.Units.SI.Inductance Lp "Parasitic Series Inductance";
    parameter Modelica.Units.SI.Resistance Ri "Insulation Resistance";
    parameter Modelica.Units.SI.Capacitance C "Capacitance";
    Modelica.Electrical.Analog.Interfaces.PositivePin p "Positive electrical pin" annotation (
      Placement(transformation(extent={{-110,-10},{-90,10}})));
    Modelica.Electrical.Analog.Interfaces.NegativePin n "Negative electrical pin" annotation (
      Placement(transformation(extent={{90,-10},{110,10}})));
    Modelica.Electrical.Analog.Basic.Capacitor capacitor(C=C) annotation(
      Placement(transformation(origin = {60, 0}, extent = {{-18, -18}, {18, 18}})));
    Modelica.Electrical.Analog.Basic.Resistor plateResistance(R=Rp) annotation(
      Placement(transformation(extent = {{-18, -18}, {18, 18}})));
    Modelica.Electrical.Analog.Basic.Resistor insulationResistance(R=Ri)  annotation(
      Placement(transformation(origin = {0, -50}, extent = {{-18, -18}, {18, 18}})));
  equation
    connect(plateResistance.n, capacitor.p) annotation(
      Line(points = {{18, 0}, {42, 0}}, color = {0, 0, 255}));
    connect(capacitor.n, n) annotation(
      Line(points = {{78, 0}, {100, 0}}, color = {0, 0, 255}));
    connect(n, insulationResistance.n) annotation(
      Line(points = {{100, 0}, {88, 0}, {88, -50}, {18, -50}}, color = {0, 0, 255}));
    connect(insulationResistance.p, p) annotation(
      Line(points = {{-18, -50}, {-88, -50}, {-88, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(plateResistance.p, p) annotation(
      Line(points = {{-18, 0}, {-100, 0}}, color = {0, 0, 255}));
    annotation(
      Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
              100}}), graphics={
          Line(points={{-6,28},{-6,-28}}, color={0,0,255}),
          Line(points={{6,28},{6,-28}}, color={0,0,255}),
          Line(points={{-90,0},{-6,0}}, color={0,0,255}),
          Line(points={{6,0},{90,0}}, color={0,0,255}),
          Text(
            extent={{-150,-40},{150,-80}},
            textString="C=%C"),
          Text(
            extent={{-150,90},{150,50}},
            textString="%name",
            textColor={0,0,255})}));
  end NonIdealCapacitor;

  model NonIdealNotchFilterModel
    parameter Modelica.Units.SI.PotentialDifference Vt(start=1) "testAmplitude";
    parameter Modelica.Units.SI.Frequency Ft(start=100000) "testFrequency";
    
    Modelica.Electrical.Analog.Basic.Ground ground annotation(
      Placement(transformation(origin = {28, -18}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Basic.Ground ground_out annotation(
      Placement(transformation(origin = {80, -30}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Sensors.VoltageSensor V_out annotation(
      Placement(transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Basic.Ground ground_in annotation(
      Placement(transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Sources.SineVoltage V_in(Vt, V(displayUnit = "V"), f(displayUnit = "Hz") = Ft) annotation(
      Placement(transformation(origin = {-60, 0}, extent = {{10, -10}, {-10, 10}})));
    
    NonIdealResistor R1(R(displayUnit = "Ohm") = 1592, Cp(displayUnit = "pF") = 5e-12, Lp(displayUnit = "uH") = -1.3e-05) annotation(
      Placement(transformation(origin = {-20, 40}, extent = {{-10, -10}, {10, 10}})));
    NonIdealResistor R2(R(displayUnit = "Ohm") = 1590, Cp(displayUnit = "pF") = 5e-12, Lp(displayUnit = "uH") = -1.27e-05) annotation(
      Placement(transformation(origin = {20, 40}, extent = {{-10, -10}, {10, 10}})));
    NonIdealResistor R3a(R(displayUnit = "Ohm") = 1594, Cp(displayUnit = "pF") = 5e-12, Lp(displayUnit = "uH") = -1.25e-05)   annotation(
      Placement(transformation(origin = {12, -18}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    NonIdealResistor R3b(R(displayUnit = "Ohm") = 1589, Cp(displayUnit = "pF") = 5e-12, Lp(displayUnit = "uH") = -1.25e-05) annotation(
      Placement(transformation(origin = {-12, -18}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    NonIdealCapacitor C1(C(displayUnit = "pF") = 9.54e-10, Rp = 23.2, Lp = -2.6, Ri(displayUnit = "GOhm") = 1e10) annotation(
      Placement(transformation(origin = {-20, -40}, extent = {{-10, -10}, {10, 10}})));
    NonIdealCapacitor C2(C(displayUnit = "pF") = 9.79e-10, Rp = 22.9, Lp(displayUnit = "mH") = -0.00258, Ri(displayUnit = "GOhm") = 1e10) annotation(
      Placement(transformation(origin = {20, -40}, extent = {{-10, -10}, {10, 10}})));
    NonIdealCapacitor C3(C(displayUnit = "pF") = 1.916e-09, Rp = 21.2, Lp(displayUnit = "mH") = -0.001321, Ri(displayUnit = "GOhm") = 1e10) annotation(
      Placement(transformation(origin = {0, 20}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  equation
    connect(R3a.n, C2.p) annotation(
      Line(points = {{12, -28}, {12, -32}, {0, -32}, {0, -40}, {10, -40}}, color = {0, 0, 255}));
    connect(R1.n, R2.p) annotation(
      Line(points = {{-10, 40}, {10, 40}}, color = {0, 0, 255}));
    connect(V_out.n, ground_out.p) annotation(
      Line(points = {{70, 0}, {80, 0}, {80, -20}}, color = {0, 0, 255}));
    connect(R1.p, C1.p) annotation(
      Line(points = {{-30, 40}, {-40, 40}, {-40, -40}, {-30, -40}}, color = {0, 0, 255}));
    connect(R2.n, C2.n) annotation(
      Line(points = {{30, 40}, {40, 40}, {40, -40}, {30, -40}}, color = {0, 0, 255}));
    connect(V_in.n, ground_in.p) annotation(
      Line(points = {{-70, 0}, {-80, 0}, {-80, -20}}, color = {0, 0, 255}));
    connect(ground.p, C3.n) annotation(
      Line(points = {{28, -8}, {28, -1}, {0, -1}, {0, 10}}, color = {0, 0, 255}));
    connect(R3b.n, C1.n) annotation(
      Line(points = {{-12, -28}, {-12, -32}, {0, -32}, {0, -40}, {-10, -40}}, color = {0, 0, 255}));
    connect(C1.n, C2.p) annotation(
      Line(points = {{-10, -40}, {10, -40}}, color = {0, 0, 255}));
    connect(C3.p, R2.p) annotation(
      Line(points = {{0, 30}, {0, 40}, {10, 40}}, color = {0, 0, 255}));
    connect(R3b.p, C3.n) annotation(
      Line(points = {{-12, -8}, {-12, -1}, {0, -1}, {0, 10}}, color = {0, 0, 255}));
    connect(V_in.p, C1.p) annotation(
      Line(points = {{-50, 0}, {-40, 0}, {-40, -40}, {-30, -40}}, color = {0, 0, 255}));
  connect(V_out.p, C2.n) annotation(
      Line(points = {{50, 0}, {40, 0}, {40, -40}, {30, -40}}, color = {0, 0, 255}));
    connect(C3.n, R3a.p) annotation(
      Line(points = {{0, 10}, {0, -1}, {12, -1}, {12, -8}}, color = {0, 0, 255}));
  end NonIdealNotchFilterModel;

  model IdealNotchFilterModel
    // Parameters
    parameter Modelica.Units.SI.PotentialDifference Vt "testAmplitude";
    parameter Modelica.Units.SI.Frequency Ft "testFrequency";
    // Components
    Modelica.Electrical.Analog.Basic.Ground ground_in annotation(
      Placement(visible = true, transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Ground ground annotation(
      Placement(transformation(origin = {28, -18}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Basic.Ground ground_out annotation(
      Placement(visible = true, transformation(origin = {80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Capacitor C1(C(displayUnit = "nF") = 1e-9, v(start = 0)) annotation(
      Placement(visible = true, transformation(origin = {-20, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Capacitor C2(C(displayUnit = "nF") = 1e-9, v(start = 0)) annotation(
      Placement(visible = true, transformation(origin = {20, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Capacitor C3(C(displayUnit = "nF") = 2e-09, v(start = 0)) annotation(
      Placement(visible = true, transformation(origin = {0, 20}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Electrical.Analog.Basic.Resistor R1(R(displayUnit = "kOhm") = 1600) annotation(
      Placement(visible = true, transformation(origin = {-20, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Resistor R2(R(displayUnit = "kOhm") = 1600) annotation(
      Placement(visible = true, transformation(origin = {20, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Resistor R3a(R(displayUnit = "kOhm") = 1600) annotation(
      Placement(transformation(origin = {12, -18}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Electrical.Analog.Basic.Resistor R3b(R(displayUnit = "kOhm") = 1600) annotation(
      Placement(transformation(origin = {-12, -18}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Electrical.Analog.Sensors.VoltageSensor V_out annotation(
      Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Sources.SineVoltage V_in(V(displayUnit = "V") = Vt, f(displayUnit = "Hz") = Ft) annotation(
      Placement(visible = true, transformation(origin = {-60, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
    // Equations
  equation
    connect(C3.n, R3a.p) annotation(
      Line(points = {{0, 10}, {0, -1}, {12, -1}, {12, -8}}, color = {0, 0, 255}));
    connect(V_out.p, C2.n) annotation(
      Line(points = {{50, 0}, {40, 0}, {40, -40}, {30, -40}}, color = {0, 0, 255}));
    connect(R1.n, R2.p) annotation(
      Line(points = {{-10, 40}, {10, 40}}, color = {0, 0, 255}));
    connect(C3.p, R2.p) annotation(
      Line(points = {{0, 30}, {0, 40}, {10, 40}}, color = {0, 0, 255}));
    connect(V_out.n, ground_out.p) annotation(
      Line(points = {{70, 0}, {80, 0}, {80, -20}}, color = {0, 0, 255}));
    connect(V_in.p, C1.p) annotation(
      Line(points = {{-50, 0}, {-40, 0}, {-40, -40}, {-30, -40}}, color = {0, 0, 255}));
    connect(R2.n, C2.n) annotation(
      Line(points = {{30, 40}, {40, 40}, {40, -40}, {30, -40}}, color = {0, 0, 255}));
    connect(V_in.n, ground_in.p) annotation(
      Line(points = {{-70, 0}, {-80, 0}, {-80, -20}}, color = {0, 0, 255}));
    connect(R1.p, C1.p) annotation(
      Line(points = {{-30, 40}, {-40, 40}, {-40, -40}, {-30, -40}}, color = {0, 0, 255}));
    connect(R3b.p, C3.n) annotation(
      Line(points = {{-12, -8}, {-12, -1}, {0, -1}, {0, 10}}, color = {0, 0, 255}));
    connect(C1.n, C2.p) annotation(
      Line(points = {{-10, -40}, {10, -40}}, color = {0, 0, 255}));
    connect(R3a.n, C2.p) annotation(
      Line(points = {{12, -28}, {12, -32}, {0, -32}, {0, -40}, {10, -40}}, color = {0, 0, 255}));
    connect(R3b.n, C1.n) annotation(
      Line(points = {{-12, -28}, {-12, -32}, {0, -32}, {0, -40}, {-10, -40}}, color = {0, 0, 255}));
    connect(ground.p, C3.n) annotation(
      Line(points = {{28, -8}, {28, -1}, {0, -1}, {0, 10}}, color = {0, 0, 255}));
    annotation(
      uses(Modelica(version = "4.0.0")),
      Diagram);
  end IdealNotchFilterModel;
  annotation(
    uses(Modelica(version = "4.0.0")));
end NotchFilter;