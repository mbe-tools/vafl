import time
import logging

import numpy
from OMPython import OMCSessionZMQ, ModelicaSystem
import itertools

import data
import experiment
import context
import simulate


def perform_experimental_activity(system: str,
                                  test_frequencies: numpy.ndarray[float],
                                  test_amplitudes: numpy.ndarray[float],
                                  sampling_ratios: numpy.ndarray[int],
                                  use_old_data: bool = True) \
        -> [float, numpy.ndarray[float]]:

    logging.basicConfig(filename=system+'.log', level=logging.INFO,
                        format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', force=True)
    experiment_frame = numpy.array(list(itertools.product(test_amplitudes, sampling_ratios, test_frequencies)))
    if system == context.SYSTEMS[0]:
        return perform_experiments(experiment_frame, use_old_data)
    else:
        return perform_simulations(system, experiment_frame, use_old_data)


def perform_experiments(experiment_frames: numpy.ndarray[float],
                        use_old_data: bool) \
        -> [float, numpy.ndarray[float]]:
    for experiment_frame in experiment_frames:
        if data.query_experiment_trace_time(experiment_frame[2], experiment_frame[0], int(experiment_frame[1])) is None or not use_old_data:
            trace_time, samples = experiment.single_experiment(experiment_frame[2], experiment_frame[0], int(experiment_frame[1]))
            if len(experiment_frames) == 1:
                return trace_time, samples
        else:
            logging.info('Found data: f:' + str(experiment_frame[2]) + ', A:' + str(experiment_frame[0]) + ', R:' + str(experiment_frame[1]))


def perform_simulations(model_name: str,
                        experiment_frames: numpy.ndarray[float],
                        use_old_data: bool) \
        -> [float, numpy.ndarray[float]]:
    omc = OMCSessionZMQ()
    model = ModelicaSystem(context.MODELICA_FILE_NAME, context.MODELICA_PACKAGE_NAME + '.' + model_name)
    model.buildModel('(V_in.v)|(V_out.v)')
    for experiment_frame in experiment_frames:
        if data.query_simulation_trace_time(experiment_frame[2], experiment_frame[0], int(experiment_frame[1]), model_name) is None or not use_old_data:
            trace_time, samples = simulate.single_simulation(model, experiment_frame[2], experiment_frame[0], int(experiment_frame[1]), model_name)
            if len(experiment_frames) == 1:
                data.delete_modelica_build_files()
                return trace_time, samples
        else:
            logging.info('Found data: f:' + str(experiment_frame[2]) + ', A:' + str(experiment_frame[0]) + ', R:' + str(experiment_frame[1]))
    data.delete_modelica_build_files()


# if __name__ == '__main__':
    # perform_experimental_activity(context.SYSTEMS[0], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([200]))
    # perform_experimental_activity(context.SYSTEMS[1], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([200]))
    # perform_experimental_activity(context.SYSTEMS[2], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([200]))
    # perform_experimental_activity(context.SYSTEMS[0], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([400]))
    # perform_experimental_activity(context.SYSTEMS[1], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([400]))
    # perform_experimental_activity(context.SYSTEMS[2], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([400]))
    # perform_experimental_activity(context.SYSTEMS[0], numpy.arange(500, 4e5, 500), numpy.arange(1.5, 5, 0.5), numpy.array([600]))
    # perform_experimental_activity(context.SYSTEMS[1], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([600]))
    # perform_experimental_activity(context.SYSTEMS[2], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([600]))
    # perform_experimental_activity(context.SYSTEMS[0], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([800]))
    # perform_experimental_activity(context.SYSTEMS[1], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([800]))
    # perform_experimental_activity(context.SYSTEMS[2], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([800]))
    # perform_experimental_activity(context.SYSTEMS[0], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([1000]))
    # perform_experimental_activity(context.SYSTEMS[1], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([1000]))
    # perform_experimental_activity(context.SYSTEMS[2], numpy.arange(500, 4e5, 500), numpy.arange(0.5, 5, 0.5), numpy.array([1000]))
