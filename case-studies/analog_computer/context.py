# systems
MODELICA_FILE_NAME: str = 'NotchFilter.mo'
MODELICA_PACKAGE_NAME: str = 'NotchFilter'
SYSTEMS: list = ['Experiment',
                 'IdealNotchFilterModel',
                 'NonIdealNotchFilterModel']

# environment
INPUT_TYPE: str = 'sin'
INPUT_OFFSET: float = 0.0
DEVICE_TYPE: str = 'AD2'
DEVICE_SN: str = ''
DEVICE_MANUFACTURER: str = 'Digilent'
# TEST_FREQUENCY: float
# TEST_AMPLITUDE: float

# workflow
OUTPUT_STABILISATION_TIME: float = 1.0
SOLVER: str = 'DASSL'
PYDWF_VERSION: str = '1.1.19'
MODELICA_VERSION: str = '1.22.0'
PYTHON_VERSION: str = '3.11'

# measurement
DATA_CYCLES: int = 10
SAMPLING_RATIO: int = 100
DECIMATION_FILTER: str = 'average'
TRIGGER_SOURCE_CHANNEL: int = 1
TRIGGER_TYPE: str = 'edge'
TRIGGER_CONDITION: str = 'rise'
TRIGGER_LEVEL: float = 0.0
TRIGGER_POSITION: float = 0.0  # time of first sample relative to trigger time
TRIGGER_HYSTERESIS: float = 0.01
CHANNEL_SIGNAL_MAPPING: dict = {0: 'output', 1: 'input'}
SOLVER_TOLERANCE: float = 1e-9  # but rounded to 8 decimal digits

#assumptions
TEMPERATURE: float = 27
ELECTROMAGNETIC_FIELD: float = 0.0
GROUND_EFFECT: float = 0.0
