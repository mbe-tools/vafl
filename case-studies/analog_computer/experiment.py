import logging
import threading

import numpy
from pydwf import DwfLibrary, DwfAnalogOutNode, DwfAnalogOutFunction, DwfAnalogInFilter, DwfAcquisitionMode, \
    DwfTriggerSource, DwfTriggerSlope, DwfEnumConfigInfo, DwfState, PyDwfError, AnalogOut, AnalogIn, DwfDevice
from pydwf.utilities import openDwfDevice
import enquiries

import data
import context


def perform_single_experiment(test_frequency1: float,
                              test_amplitude1: float,
                              test_frequency2: float,
                              test_amplitude2: float,
                              phase_diff: float,
                              system: str) \
        -> [numpy.ndarray[float], list[str]]:
    logging.basicConfig(filename=data.DATA_DIRECTORY + '/' + system + '.log', level=logging.INFO,
                        format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', force=True)
    trace, legend = single_experiment(test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff)
    if bool(enquiries.choose('save experiment data?', ['True', 'False'])):
        data.save_experiment_trace(trace, test_frequency1, test_amplitude1, test_frequency2, test_amplitude2,
                                   phase_diff, legend, system)
    return trace, legend


def single_experiment(test_frequency1: float,
                      test_amplitude1: float,
                      test_frequency2: float,
                      test_amplitude2: float,
                      phase_diff: float,
                      ch3_record: bool = False,
                      ch3_name: str = None,
                      ch3_data_flag: bool = False) \
        -> [numpy.ndarray[float], list[str]]:
    logging.info('Performing experiment: f1:' + str(test_frequency1) + ', A1:' + str(test_amplitude1) +
                 ', f2:' + str(test_frequency2) + ', A2:' + str(test_amplitude2) + ', phi:' + str(phase_diff))
    dwf: DwfLibrary = DwfLibrary()

    def maximize_buffer(configuration_parameters):
        return configuration_parameters[DwfEnumConfigInfo.AnalogInBufferSize]

    try:
        with openDwfDevice(dwf, serial_number_filter='F2', score_func=maximize_buffer) as device0:
            with openDwfDevice(dwf, serial_number_filter='F4', score_func=maximize_buffer) as device1:
                device0.triggerSet(0, DwfTriggerSource.PC)
                configure_output(device0.analogOut, test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff)
                if not ch3_data_flag:
                    ch3_record: bool = eval(str(enquiries.choose('record 4th signal?', ['True', 'False'])))
                    if ch3_record:
                        ch3_name = input('4th signal name (ideally corresponds to name in cir file): ')
                    ch3_data_flag = True
                sampling_frequency: float = max([test_frequency1, test_frequency2]) * context.SAMPLING_RATIO
                sampling_time: float = 10 / min([test_frequency1, test_frequency2])
                configure_input(device0.analogIn, device1.analogIn, ch3_record, sampling_frequency)
                samples, legend = record_experiment(device0.analogIn, device1.analogIn, device0, ch3_record, ch3_name, sampling_frequency, sampling_time)
                return samples, legend
    except (PyDwfError, RuntimeError) as exception:
        logging.warning(exception)
        return single_experiment(test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff, ch3_record, ch3_name, ch3_data_flag)


def configure_output(analog_output_device: AnalogOut,
                     test_frequency1: float,
                     test_amplitude1: float,
                     test_frequency2: float,
                     test_amplitude2: float,
                     phase_diff: float) \
        -> None:
    ch0: int = 0
    ch1: int = 1
    node = DwfAnalogOutNode.Carrier

    analog_output_device.reset(-1)

    analog_output_device.nodeEnableSet(ch0, node, True)
    analog_output_device.nodeFunctionSet(ch0, node, DwfAnalogOutFunction.Sine)
    analog_output_device.nodeFrequencySet(ch0, node, test_frequency1)
    analog_output_device.nodeAmplitudeSet(ch0, node, test_amplitude1)
    analog_output_device.nodeOffsetSet(ch0, node, 0)
    analog_output_device.nodePhaseSet(ch0, node, 0)
    analog_output_device.triggerSourceSet(ch0, DwfTriggerSource.External1)
    analog_output_device.triggerSlopeSet(ch0, DwfTriggerSlope.Rise)

    analog_output_device.nodeEnableSet(ch1, node, True)
    analog_output_device.nodeFunctionSet(ch1, node, DwfAnalogOutFunction.Sine)
    analog_output_device.nodeFrequencySet(ch1, node, test_frequency2)
    analog_output_device.nodeAmplitudeSet(ch1, node, test_amplitude2)
    analog_output_device.nodeOffsetSet(ch1, node, 0)
    analog_output_device.nodePhaseSet(ch1, node, phase_diff)
    analog_output_device.triggerSourceSet(ch1, DwfTriggerSource.External1)
    analog_output_device.triggerSlopeSet(ch1, DwfTriggerSlope.Rise)

    analog_output_device.configure(-1, 1)


def configure_input(analog_input_device0: AnalogIn,
                    analog_input_device1: AnalogIn,
                    ch3_enable: bool,
                    sampling_frequency: float) \
        -> None:
    ch0: int = 0  # reads input 1
    ch1: int = 1  # reads input 2
    ch2: int = 0  # reads output
    ch3: int = 1  # reads supplementary

    analog_input_device0.reset()
    analog_input_device1.reset()

    analog_input_device0.channelEnableSet(ch0, True)
    analog_input_device0.channelEnableSet(ch1, True)
    analog_input_device1.channelEnableSet(ch2, True)

    analog_input_device0.channelFilterSet(ch0, DwfAnalogInFilter.Average)
    analog_input_device0.channelFilterSet(ch1, DwfAnalogInFilter.Average)
    analog_input_device1.channelFilterSet(ch2, DwfAnalogInFilter.Average)

    if ch3_enable:
        analog_input_device1.channelEnableSet(ch3, ch3_enable)
        analog_input_device1.channelFilterSet(ch3, DwfAnalogInFilter.Average)

    analog_input_device0.acquisitionModeSet(DwfAcquisitionMode.Record)
    analog_input_device1.acquisitionModeSet(DwfAcquisitionMode.Record)

    analog_input_device0.recordLengthSet(-1)
    analog_input_device1.recordLengthSet(-1)

    analog_input_device0.frequencySet(sampling_frequency)
    analog_input_device1.frequencySet(sampling_frequency)

    analog_input_device0.triggerSourceSet(DwfTriggerSource.External1)
    analog_input_device1.triggerSourceSet(DwfTriggerSource.External1)

    analog_input_device0.triggerPositionSet(0)
    analog_input_device1.triggerPositionSet(0)


def record_experiment(analog_input_device0: AnalogIn,
                      analog_input_device1: AnalogIn,
                      trigger_device: DwfDevice,
                      ch3_enable: bool,
                      ch3_name: str,
                      sampling_frequency: float,
                      sampling_time: float) \
        -> [numpy.ndarray[float], list[str]]:
    num_samples: int = int(sampling_frequency * sampling_time)
    input1_samples: list = []
    input2_samples: list = []
    output_samples: list = []
    ch3_samples: list = []
    analog_input_device0.configure(True, True)
    analog_input_device1.configure(True, True)

    thread_device0 = threading.Thread(target=read_device, args=(analog_input_device0, num_samples, 0, input1_samples, input2_samples))
    thread_device1 = threading.Thread(target=read_device, args=(analog_input_device1, num_samples, 1, output_samples, ch3_samples))
    thread_device0.daemon = True
    thread_device1.daemon = True

    try:
        trigger_device.triggerPC()
        thread_device0.start()
        thread_device1.start()
        thread_device0.join()
        thread_device1.join()
    except RuntimeError as exception:
        thread_device0.join()
        thread_device1.join()
        logging.warning(exception)
        raise

    legend = ['time', 'input1', 'input2', 'output']

    if ch3_enable:
        legend.append(ch3_name)
        return numpy.array([numpy.arange(0, num_samples / sampling_frequency, 1 / sampling_frequency),
                            numpy.concatenate(input1_samples, axis=None)[:num_samples],
                            numpy.concatenate(input2_samples, axis=None)[:num_samples],
                            numpy.concatenate(output_samples, axis=None)[:num_samples],
                            numpy.concatenate(ch3_samples, axis=None)[:num_samples]]), legend
    else:
        return numpy.array([numpy.arange(0, num_samples / sampling_frequency, 1 / sampling_frequency),
                            numpy.concatenate(input1_samples, axis=None)[:num_samples],
                            numpy.concatenate(input2_samples, axis=None)[:num_samples],
                            numpy.concatenate(output_samples, axis=None)[:num_samples]]), legend


def read_device(analog_input_device: AnalogIn,
                num_samples: int,
                device_number: int,
                input0_samples: list,
                input1_samples: list)\
        -> [list[float], list[float]]:
    acquired_samples: int = 0

    while True:
        status = analog_input_device.status(True)
        current_samples, current_samples_lost, current_samples_corrupted = analog_input_device.statusRecord()

        if current_samples_lost + current_samples_corrupted != 0:
            raise RuntimeError('Samples lost/corrupted: (' + str(current_samples_lost) + ',' + str(
                    current_samples_corrupted) + ')')

        if current_samples != 0:
            input0_samples.append(analog_input_device.statusData(0, current_samples))
            input1_samples.append(analog_input_device.statusData(1, current_samples))
            logging.info('Acquired ' + str(current_samples) + ' samples from device ' + str(device_number))
            acquired_samples = acquired_samples + current_samples

        if status == DwfState.Done or acquired_samples >= num_samples:
            break
