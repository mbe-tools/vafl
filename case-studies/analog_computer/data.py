import csv
import logging
import os
import shutil
import signal
import sys
from datetime import datetime
from os import path

import numpy
import pandas


DATA_DIRECTORY = 'data'
DATA_FILE_EXTENSION: str = '.csv'
ARCHITECTURE_FILE_EXTENSION: str = '.cir'
STOP: bool = False
header_row: list[str] = ['time', 'test_frequency1', 'test_amplitude1', 'test_frequency2', 'test_amplitude2', 'phase_diff', 'type']


def query_systems() -> list[str]:
    systems = []
    files: list[str] = os.listdir(DATA_DIRECTORY)
    for file in files:
        if file.endswith('cir'):
            systems.append(file[:-4])
    return systems


def query_experiment_trace_times(test_frequency1: float,
                                 test_amplitude1: float,
                                 test_frequency2: float,
                                 test_amplitude2: float,
                                 phase_diff: float,
                                 system: str) \
        -> list[float] | None:
    file_address: str = DATA_DIRECTORY + '/' + system + DATA_FILE_EXTENSION
    if path.isfile(file_address):
        all_metadata: pandas.DataFrame = pandas.read_csv(file_address, header=0, usecols=['time', 'test_frequency1', 'test_amplitude1', 'test_frequency2', 'test_amplitude2', 'phase_diff', 'type'])
        required_names: list[float] = (all_metadata.loc[(all_metadata['test_frequency1'] == test_frequency1) &
                                                         (all_metadata['test_amplitude1'] == test_amplitude1) &
                                                         (all_metadata['test_frequency2'] == test_frequency2) &
                                                         (all_metadata['test_amplitude2'] == test_amplitude2) &
                                                         (all_metadata['phase_diff'] == phase_diff) &
                                                         (all_metadata['type'] == 'output'), 'time']).tolist()
        if len(required_names) > 0:
            return required_names
        else:
            return None
    return None


def query_trace_from_time(trace_time: float, system: str) \
        -> [numpy.ndarray[float], list[str]]:
    file_address: str = DATA_DIRECTORY + '/' + system + DATA_FILE_EXTENSION
    samples = []
    legend = []
    with open(file_address) as data_file:
        header_row = next(data_file)
        reader = csv.reader(data_file)
        for row in reader:
            if row[0] == str(trace_time):
                legend.append(row[6])
                samples.append(row[7:])
    return numpy.float_(samples), legend


def save_experiment_trace(trace: numpy.ndarray[float],
                          test_frequency1: float,
                          test_amplitude1: float,
                          test_frequency2: float,
                          test_amplitude2: float,
                          phase_diff: float,
                          legend: list[str],
                          system: str):

    def interrupt_handler(signal, frame):
        global STOP
        STOP = True
        logging.warning('termination signal encountered')
    signal.signal(signal.SIGINT, interrupt_handler)
    signal.signal(signal.SIGTERM, interrupt_handler)

    trace_time: float = datetime.timestamp(datetime.now())
    file_name: str = DATA_DIRECTORY + '/' + system + DATA_FILE_EXTENSION
    if not os.path.isfile(file_name):
        shutil.copyfile(system + ARCHITECTURE_FILE_EXTENSION,
                        DATA_DIRECTORY + '/' + system + ARCHITECTURE_FILE_EXTENSION)
    add_header_flag: bool = True
    if os.path.isfile(file_name):
        add_header_flag = False
    with open(file_name, "a+") as file:
        global STOP
        if not STOP:
            writer = csv.writer(file)
            if add_header_flag:
                writer.writerow(header_row)
            for i in range(len(trace)):
                row: list = [trace_time, test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff, legend[i]]
                row.extend(trace[i])
                writer.writerow(row)
            file.close()
        else:
            file.close()
            sys.exit()
