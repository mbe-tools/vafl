import os.path

import matplotlib
from matplotlib import pyplot
import numpy
import enquiries

import experiment
import data

matplotlib.use('TkAgg')


def plot_trace(data: list[numpy.ndarray[float]],
               legend: list[str]) \
        -> None:
    x_data = data[0]
    print(x_data)
    figure, axis = pyplot.subplots()
    for signal in data[1:]:
        print(signal)
        axis.plot(x_data, signal)
    pyplot.xlabel('Time (s)')
    pyplot.ylabel('Voltage (V)')
    pyplot.legend(legend[1:])
    pyplot.show()


def user_choice(question: str,
                options: list,
                multi: bool = False) \
        -> str | list[str]:
    response = enquiries.choose(question, options, multi=multi)
    if multi:
        for option in response:
            print(question + ': ' + option)
    else:
        print(question + ': ' + response)
    return response


if __name__ == "__main__":
    system_options = data.query_systems()
    system_options.append('other')
    system: str = user_choice('data from which system/s?', system_options)
    if system == 'other':
        while True:
            system = input('.cir file name for new system: ')
            if os.path.isfile(system+'.cir'):
                break
    test_frequency1: float = float(input('Input signal 1 frequency (Hz): '))
    test_amplitude1: float = float(input('Input signal 1 amplitude (V): '))
    test_frequency2: float = float(input('Input signal 2 frequency (Hz): '))
    test_amplitude2: float = float(input('Input signal 2 amplitude (V): '))
    phase_diff: float = float(input('Phase difference between input signals <ph(sig2) - ph(sig1)> (deg): ')) % 360
    trace_times: list[float] = data.query_experiment_trace_times(test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff, system)
    if trace_times is None:
        trace, legend = experiment.perform_single_experiment(test_frequency1, test_amplitude1, test_frequency2, test_amplitude2, phase_diff, system)
        plot_trace(trace, legend)
    else:
        trace_options = [str(i) for i in trace_times]
        trace_options.append('perform new experiment..')
        trace_time = user_choice('multiple experiments found. select an experiment.', trace_options)
        if trace_time == 'perform new experiment..':
            trace, legend = experiment.perform_single_experiment(test_frequency1, test_amplitude1, test_frequency2,
                                                                 test_amplitude2, phase_diff, system)
            plot_trace(trace, legend)
        else:
            trace, legend = data.query_trace_from_time(float(trace_time), system)
            plot_trace(trace, legend)
