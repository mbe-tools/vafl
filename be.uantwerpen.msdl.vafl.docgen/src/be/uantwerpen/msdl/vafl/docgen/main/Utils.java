package be.uantwerpen.msdl.vafl.docgen.main;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.resource.Resource;

import io.opencaesar.oml.Entity;
import io.opencaesar.oml.Import;
import io.opencaesar.oml.RelationEntity;
import io.opencaesar.oml.ScalarProperty;
import io.opencaesar.oml.Vocabulary;
import io.opencaesar.oml.VocabularyBundle;

public class Utils {

	public static List<VocabularyBundle> getAllVocabularyBundles (VocabularyBundle bundle) {
		List<VocabularyBundle> returnList = new ArrayList<VocabularyBundle>();
		for (Resource resource : bundle.eResource().getResourceSet().getResources()) {
			if (resource.getContents().get(0).eClass().getName().matches("VocabularyBundle")) {
				returnList.add((VocabularyBundle) resource.getContents().get(0));
			}
		}
		return returnList;
	}
	
	public static List<Vocabulary> getAllVocabularies (VocabularyBundle bundle) {
		List<Vocabulary> returnList = new ArrayList<Vocabulary>();
		for (Resource resource : bundle.eResource().getResourceSet().getResources()) {
			if (resource.getContents().get(0).eClass().getName().matches("Vocabulary")) {
				returnList.add((Vocabulary) resource.getContents().get(0));
			}
		}
		return returnList;
	}
	
	public static String escapeLatexSpecialChars (String string) {
		string = string.replaceAll("#","\\\\#");
		return string;
	}
	
	public static List<String> getRelationAxioms (RelationEntity relation) {
		List<String> returnList = new ArrayList<String>();
		if (relation.isAsymmetric()) {returnList.add("asymmetric");}
		if (relation.isFunctional()) {returnList.add("functional");}
		if (relation.isInverseFunctional()) {returnList.add("inverse functional");}
		if (relation.isIrreflexive()) {returnList.add("irreflexive");}
		if (relation.isReflexive()) {returnList.add("reflexive");}
		if (relation.isSymmetric()) {returnList.add("symmetric");}
		if (relation.isTransitive()) {returnList.add("transitive");}
		return returnList;
	}
	public static Boolean isNotEmptyAxiom (RelationEntity relation) {
		if (relation.isAsymmetric() || relation.isFunctional() || relation.isInverseFunctional() || relation.isIrreflexive() ||
		    relation.isReflexive() || relation.isSymmetric() || relation.isTransitive()) {
		    	return true;
		    }
		return false;
	}
	
	public static List<Import> getExtensionImports (VocabularyBundle bundle) {
		List<Import> returnList = new ArrayList<Import>();
		for (Import vocabImport : bundle.getOwnedImports()) {
			if (vocabImport.getKind().getName().matches("extension")) {
				returnList.add(vocabImport);
			}
		}
		return returnList;
	}
	
	public static List<Import> getInclusionImports (VocabularyBundle bundle) {
		List<Import> returnList = new ArrayList<Import>();
		for (Import vocabImport : bundle.getOwnedImports()) {
			if (vocabImport.getKind().getName().matches("inclusion")) {
				returnList.add(vocabImport);
			}
		}
		return returnList;
	}
	
	public static Boolean hasEntityDomain (ScalarProperty property, Entity entity) {
		return property.getDomains().contains(entity);
	}
}
