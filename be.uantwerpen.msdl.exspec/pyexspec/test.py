import exspec

text = """
notchfilter_experiment = dwf_experiment MERGE
'''This is an experiment conducted to record the output of the notch-\
filter when subject to pure tones with DWF technology'''
# dwf_experiment::dwf_experiment MERGE <- could have used this instead of list-level merge
{
    system = {
        id: str = '987364' # refers to notch_filter ci in knowledge graph
        _manufacturer: str = 'MSDL'
    }
    environment.device.output.channel0 = {
        type: str = 'sin'
        test_frequency: T-1 = RANGE(500, 4e5, 500) Hz
        test_amplitude: ML2T-3I-1 = RANGE(0.5, 5, 0.5) V
        offset: ML2T-3I-1 = 0 V
    }
    workflow = {
        atomic = {
            record =  {
                sampling_ratio: int = RANGE(200, 1200, 200)
                data_cycles: int = 2
                decimation_filter: str = 'average'
                trigger = {
                    source: str = 'environment.output'
                    type: str = 'edge'
                    condition: str = 'rise'
                    level: ML2T-3I-1 = 0 V
                    position: T = 0 s
                    hysteresis: ML2T-3I-1 = 0.01 V
                }
            }
            stabilisation_time: T = 1 s
            initial = {
                environment.output: ML2T-3I-1 = 0 V
                system.charge: TI = 0 C
            }
        }
    }
    architecture: list[str] = ['environment.output => system.input',
                               'system.output => environment.input.channel0',
                               'environment.output => environment.input.channel1']
}
"""

print(exspec.parser.parse(text).pretty())
