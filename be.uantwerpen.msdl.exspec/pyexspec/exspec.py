from lark import Lark
grammar = """
start: specification+

specification: EXSPEC_NAME "=" formula

EXSPEC_NAME: CNAME

%import common.CNAME

formula: (dictionary OPERATION)* dictionary

OPERATION: "OR"
         | "MERGE" 

dictionary: dictionary_ref 
          | dictionary_def
          | "(" formula ")"

dictionary_ref: (PACKAGE_NAME "::")? EXSPEC_NAME

PACKAGE_NAME: CNAME

dictionary_def: "{" proposition+ "}"

proposition: PROPERTY_NAME (":" property_type)? "=" property_value unit?

PROPERTY_NAME: (CNAME".")* CNAME

property_type: BASIC_TYPE | composed_type | physical_type

BASIC_TYPE: "str" | "int" | "float"

composed_type: "list[" property_type "]"

physical_type: (dimension SIGNED_INT?)+

%import common.SIGNED_INT

dimension: "M" -> mass
         | "L" -> length
         | "T" -> time
         | "I" -> current
         | "K" -> temperature
         | "N" -> substance
         | "J" -> luminosity 

property_value: dictionary_def 
              | "RANGE(" START "," END ("," STEP)? ")" -> range
              | "[" (basic_property_value ";")* basic_property_value "]" -> set
              | basic_property_value

START: SIGNED_NUMBER

END: SIGNED_NUMBER

STEP: SIGNED_NUMBER

basic_property_value: basic_value 
                    | "[" (basic_value ",")* basic_value "]" -> list

basic_value: SIGNED_NUMBER -> number
           | /'.*'/ -> string

%import common.SIGNED_NUMBER

unit: WORD 
    | "n-" WORD -> nano
    | "u-" WORD -> micro
    | "m-" WORD -> milli
    | "k-" WORD -> kilo
    | "M-" WORD -> mega
    | "G-" WORD -> giga
    | "T-" WORD -> terra

%import common.WORD

COMMENT: /#.*/
MULTI_LINE_COMMENT: /'''[^.]*'''/

%import common.WS
%ignore WS
%ignore COMMENT
%ignore MULTI_LINE_COMMENT
"""
parser = Lark(grammar)
